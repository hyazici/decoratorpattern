﻿using System.Collections.Generic;

namespace FlightSearch.Driver.Classes
{
    public class IataSearch : IFlight
    {
        private List<FlightSearchResult> _results;

        public List<FlightSearchResult> Search(FlightSearchCriteria criteria)
        {
            _results = new List<FlightSearchResult>
            {
                new FlightSearchResult
                {
                    FromCity = "AYT",
                    ToCity = "IST",
                    PnlName = "FL0001"
                }
            };

            return _results;
        }
    }
}
