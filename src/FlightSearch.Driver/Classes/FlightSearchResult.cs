﻿namespace FlightSearch.Driver.Classes
{
    public class FlightSearchResult
    {
        public string FromCity { get; set; }
        public string ToCity { get; set; }
        public string PnlName { get; set; }
        public decimal? Price { get; set; }
    }
}
